package com.abhi;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        PalindromeChecker palindromeChecker = new PalindromeChecker();
        palindromeChecker.setFileObject(new FileInput());
        boolean result = palindromeChecker.Check();
        if(result) System.out.println("Palindrome");
        else System.out.println("Not a Palindrome");
    }
}

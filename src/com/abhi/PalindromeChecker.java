package com.abhi;
import java.io.IOException;
//Change the class name to a meaningfull one
public class PalindromeChecker {

    private FileInput fileInput;

    public void setFileObject(FileInput file){
        this.fileInput = file;
    }
    // Change the function name
    public boolean Check() throws IOException {

        String str = fileInput.readFile("input.txt"); //why 2 semi-colons
//        System.out.println(str);
        int i = 0, j = str.length() - 1;
        boolean chk = true;
        while (i < j) {
            if (str.charAt(i) != str.charAt(j)) {
                chk = false;
                break;
            }
            i++;
            j--;
        }
        if(chk) return true;
        else return false;
    }
}

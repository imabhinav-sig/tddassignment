package com.abhi.palintools;

import com.abhi.FileInput;
import com.abhi.PalindromeChecker;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Spy;

import java.io.IOException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

// Mocking file object and testing

public class PalindromeTest {

    @Mock
    FileInput file;
    PalindromeChecker palindromeChecker;
    PalindromeChecker palindromeCheckerNew;

    @Spy
    FileInput newFile;

    @Before
    public void setup(){
//        System.out.println("Setup Running");
        file = mock(FileInput.class);
        palindromeChecker = new PalindromeChecker();
        palindromeCheckerNew = new PalindromeChecker();
        palindromeChecker.setFileObject(file);
        newFile = spy(new FileInput());
        palindromeCheckerNew.setFileObject(newFile);
    }

    @Test
    public void verifyFileInputCalled() throws IOException {
        when(file.readFile(anyString())).thenReturn("abcddcba");
        boolean result = palindromeChecker.Check();
        assertTrue(result); //why is this commented?
        verify(file).readFile(anyString());
    }

    @Test
    public void fileInputValidPalindrome() throws IOException {
        when(file.readFile(anyString())).thenReturn("abcddcba");
        boolean result = palindromeChecker.Check();
        assertTrue(result);
//        verify(file).readFile("input.txt");
    }

    @Test
    public void fileInputInValidPalindrome() throws IOException {
        doReturn("qwerty").when(newFile).readFile(anyString());
        boolean result = palindromeCheckerNew.Check();
//        verify(file).readFile(anyString());
        assertFalse(result);
    }

    //what if the file is empty or corrupted?
}

